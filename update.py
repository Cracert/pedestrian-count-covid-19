#############################################
# Object detection - YOLO - OpenCV
# Author : Arun Ponnusamy   (July 16, 2018)
# Website : http://www.arunponnusamy.com
#
# Modified by Robert Szczepanek (October, 2020)
# https://gitlab.com/Cracert/pedestrian-count-covid-19
#
# conda activate cv
############################################


import cv2
import numpy as np
import datetime
from pathlib import Path
from operator import add
import glob
import argparse

from data import write_count, read_count, n_first_classes
from data_source import camera

C_WHITE = (255,255,255)
C_GRAY = (200,200,200)
C_GREEN = (0, 200, 0)

config_file = 'yolov3.cfg'
weights_file = 'data/yolov3.weights'
classes_file = 'yolov3.txt'


def parse_args():
    """ Parse script call parameters.

    :param: None
    :return: int -- year to be updated
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("-y", help="Year to be updated.", type=int)
    args = parser.parse_args()
#    print(f"Updating for year {args.y}")

    return args.y


def get_output_layers(net):

    layer_names = net.getLayerNames()
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    return output_layers


def draw_prediction(img, class_id, confidence, x, y, x_plus_w, y_plus_h, i,
                    classes, COLORS):

    label = str(classes[class_id])
    color = COLORS[class_id]
    cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 1)
    cv2.putText(img, label, (x-5,y-5),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1)


def draw_frames(img, split=3):

    width = img.shape[1]
    height = img.shape[0]

    vertical = int(height / 2)
    horizontal = []
    for s in (1, split-1):
        horizontal.append(int(width / split * s))
    cv2.line(img, (0, vertical), (width, vertical), C_GRAY)
    for h in horizontal:
        cv2.line(img, (h, 0), (h, height), C_GRAY)


def cut_frames(img):
    # default 3 cols x 2 rows

    width = img.shape[1]
    height = img.shape[0]
    frame_w = int(width / 3.)
    frame_h = int(height / 2.)
    frames = []

    for row in range(2):
        for col in range(3):
            frame = img[row * frame_h: (row+1) * frame_h,
                        col * frame_w: (col+1) * frame_w]
            frames.append(frame)

    return frames


def detect(image_raw, split=None, result_name=None, verbose=False):

    width = image_raw.shape[1]
    height = image_raw.shape[0]
    scale = 0.00392

    image = image_raw.copy()

    classes = None

    with open(classes_file, 'r') as f:
        classes = [line.strip() for line in f.readlines()]

    COLORS = np.full((len(classes), 3), 255, dtype=float)
    color = ((0, C_GREEN, 'person'),
             (2, [0, 100, 200], 'car'),
             (7, [0, 0, 200], 'truck'))
    for c in color:
        COLORS[c[0]] = c[1]

    net = cv2.dnn.readNet(weights_file, config_file)
    blob = cv2.dnn.blobFromImage(image, scale, (416,416), (0,0,0),
                                 True, crop=False)
    net.setInput(blob)
    outs = net.forward(get_output_layers(net))

    class_ids = []
    confidences = []
    boxes = []
    conf_threshold = 0.5
    nms_threshold = 0.4

    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])

    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

    count_class = [0] * len(classes)

    for i in indices:
        i = i[0]
        box = boxes[i]
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]
        draw_prediction(image, class_ids[i], confidences[i], round(x), round(y),
                        round(x+w), round(y+h), int(i), classes, COLORS)
        if split:
            draw_frames(image, split)
        count_class[class_ids[i]] = count_class[class_ids[i]] + 1

    if verbose:
        txt = ''
        for i in range(20):
            if count_class[i] > 0:
                txt += ' {}:{}'.format(classes[i], count_class[i])
        print(txt)

    if result_name:
        result_name = result_name + '.jpg'
        print(result_name)
        cv2.imwrite(result_name, image)

    return count_class


def detect_in_folder(folder, split=False):
    # detect peestrians on all images in folder

    for image_file in glob.glob(folder + '*.jpg'):
        image_raw = cv2.imread(image_file)
        if not split:
            result_name = 'img/' + image_file[len(folder):-4] + '-yolo'
            detect(image_raw, split=None, result_name=result_name)
        else:
            imgs = cut_frames(image_raw)
            i = 1
            for img in imgs:
                result_name = 'img/' + image_file[len(folder):-4] + \
                    f'-yolo-tile_{i}'
                i += 1
                    #count_class_frame = detect(img, split=None,
                   #    result_name=f'img_tmp/{date}_{h:02}-00_{i}')
                detect(img, split=None, result_name=result_name)

def main_frames():

    split = 2
    folder = 'data/'
    f = 'krakow_cam_.stream_2019-07-02_17-00.jpg'
    image_file = folder + f
    result_file = f.split('.')
    result_file.insert(-1, f'split{split}')
    result_file = '.'.join(result_file)

    image_raw = cv2.imread(image_file)
    detect(image_raw, split, result_file)


def main(split=None):

    folder = '/home/robert/home2/prj/2016/haufar/haufar/data/webcamera/'
    names = camera()

    for name in names:
        place = name
        camera_folder = names[place]['folder']
        camera_appendix = names[place]['appendix']
        stat_file = names[place]['file']
        first_frame = names[place]['first_frame']
        last_frame = names[place]['last_frame']

        year_todo = parse_args()
        if not year_todo:
            year_todo = datetime.datetime.now().year
        try:
            df = read_count(f'data/{stat_file}-{year_todo}-split6.csv')
            last_month = int(df.tail(1)['date'].dt.month)
            last_day = int(df.tail(1)['date'].dt.day)
            date = datetime.date(year_todo, last_month, last_day)
        except FileNotFoundError:   # create file and write header
            write_count(f'data/{stat_file}-{year_todo}-split6.csv', None)
            last_month = 12
            last_day = 31
            date = datetime.date(year_todo - 1, last_month, last_day)

        day1 = datetime.timedelta(days=1)
        date += day1

        data = []

# TODO - don't stop within one day
        while date < datetime.date(year_todo+1, 1, 1):
#        while date < datetime.date(year_todo, 12, 1):
            year = date.year
            print(name, date)
            for h in range(0, 24):
                f = f'{date}_{h:02}-00.jpg'
                image_file = f'{folder}{camera_folder}/{year}/{camera_appendix}{f}'
                if Path(image_file).is_file():
                    image_raw = cv2.imread(image_file)
                    if image_raw is not None:
                        if not split:
                            count_class = detect(image_raw)
                            count_class.insert(0, f'{date}_{h:02}-00')
                            data.append(count_class)
                        else:
                            imgs = cut_frames(image_raw)
                            i = 1
                            count_class = [0] * n_first_classes

                            for img in imgs[first_frame:last_frame]:
                                # save frame images
                                #count_class_frame = detect(img, split=None,
                                #    result_name=f'img_tmp/{date}_{h:02}-00_{i}')
                                count_class_frame = detect(img, split=None)
                                i += 1
                                count_class = list(map(add, count_class,
                                                       count_class_frame))
                            count_class.insert(0, f'{date}_{h:02}-00')
                            data.append(count_class)


                    # incremental write - every hour
                    # write_count(f'data/dane3.csv',[count_class])

            # incremental write - every day
            if not split:
                write_count(f'data/{stat_file}-{year}.csv', data)
            else:
                write_count(f'data/{stat_file}-{year}-split6.csv', data)
            data = []

            date=date + day1


if __name__ == '__main__':

    main(split=True)
