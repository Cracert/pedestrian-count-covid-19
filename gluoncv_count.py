#############################################
# Object detection using GluonCV
# Website : https://gluon-cv.mxnet.io/build/examples_detection/index.html
#
# Author : Robert Szczepanek (July, 2020)
# https://gitlab.com/Cracert/pedestrian-count-covid-19
############################################

import mxnet as mx
import gluoncv as gcv
import numpy as np

import datetime
from pathlib import Path

from data import write_count
from data_source import camera

import logging
logging.basicConfig(level=logging.INFO,
                    filename='data/info.log',
                    format='%(asctime)s - %(message)s')


def count_people(ndarray, person_id=None):

    idx = np.where(ndarray != -1)[0]
    id_list = ndarray[idx]

    x = np.where(id_list == person_id)

    if x[0].size > 0:
        return len(id_list[np.where(id_list == person_id)[0]])
    else:
        return 0


def main(model=None):

    folder = '/home/robert/home2/prj/2016/haufar/haufar/data/webcamera/'

    names = camera()
    place = 'All Saints Square'
#    place = 'Grodzka'
#    place = 'Wawel'
#    place = 'Podgorze Market Square'

    camera_folder = names[place]['folder']
    camera_appendix = names[place]['appendix']
    stat_file = names[place]['file']

    year_todo = 2020
    date = datetime.date(year_todo, 3, 1) # verification
    day1 = datetime.timedelta(days=1)

    logging.info(f'Starting {model} for day {date} at location {place}.')

    data = []

    while date < datetime.date(year_todo, 4, 1):  # verification period 1-31.03.2020
        year = date.year
        for h in range(0, 24):
            print(f'{date} {h:02}:00')
            f = f'{date}_{h:02}-00.jpg'
            image_file = f'{folder}{camera_folder}/{year}/{camera_appendix}{f}'
            if Path(image_file).is_file():
                if Path(image_file).stat().st_size > 300:   # small, corrupted files < 300B
                    try:
                        image_raw = mx.image.imread(image_file)
                    except:
                        continue
                else:
                    logging.info(f'Problem with {image_file} file.')
                    continue

                if image_raw is not None:
#                     https://gluon-cv.mxnet.io/api/model_zoo.html
                    if model == 'FasterRCNN':
                        image, chw_image = gcv.data.transforms.presets.rcnn.transform_test(image_raw)
                        network = gcv.model_zoo.get_model('faster_rcnn_resnet50_v1b_voc',pretrained=True)
                        person_class_id = 14
                    elif model == 'SSD':
                        image, chw_image = gcv.data.transforms.presets.ssd.transform_test(image_raw, short=512)
                        network = gcv.model_zoo.get_model('ssd_512_resnet50_v1_voc',pretrained=True)
                        person_class_id = 15

                    prediction = network(image)
                    prediction = [array[0] for array in prediction] #since we only input 1 image
                    class_indices, probabilities, bounding_boxes = prediction #unpacking

                    n = count_people(class_indices, person_class_id)
                    count_class = [n] + [0]*8  # save 0 for other classes
                    count_class.insert(0, f'{date}_{h:02}-00')
                    data.append(count_class)
#                    print(network.classes)
            else:
                logging.info(image_file + ' is not file')

        # incremental write - every day
        write_count(f'data/{stat_file}-{year}-{model}.csv', data)
        data = []

        date=date + day1

    logging.info(f'Ending {model} for day {date} at location {place}.')


if __name__ == '__main__':
    main(model='FasterRCNN')
    main(model='SSD')
