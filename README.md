# Pedestrian detection and counting before and during COVID-19

Pedestrian detection using deep learning model (YOLOv3/DarkNet) from OpenCV and Python3. Time-lapse images are from four webcams in Cracow (Poland) https://www.webcamera.pl/.


 ## Dependencies
  * python3.5
  * opencv
  * numpy
  * pandas

  
Download the pre-trained YOLOv3 weights file from https://pjreddie.com/media/files/yolov3.weights (250MB) and place it in `data` folder.
 
 `$ wget https://pjreddie.com/media/files/yolov3.weights`
 
 ## Content
 
 #### Scripts 

  * `yolo_count.py` - main script
  * `data_source.py` - config file with link to time-lapse folders (just template)
  * `analysis-method.ipynb` - Jupyter notebook for two YOLO versions comparison
  * `analysis-pedestrians.ipynb` - Jupyter notebook for analysis of numeric data
  * `data.py` - functions for notebooks

#### Data

* `/data` - CSV files with detected number of pedestrians and eight other objects; hourly from years 2016-2020
* `/figure` - figures generated for the paper
* `/img/max_difference` - sample images with maximum difference between two methods: YOLOv3 and YOLOv3-tiled; folder `/img` contains images with results

  
 ## Run
  
 `$ python yolo_count.py` to test detection of images from `/img/max_difference` folder. 
  
## Figure updated with recent data

![Figure with recent data](figure/pedestrian_activity_change_weekly_2020-11-30.png?raw=true)

## How to cite

Szczepanek R. 2020. Analysis of pedestrian activity before and during COVID-19 lockdown, using webcam time-lapse from Cracow and machine learning. PeerJ 8:e10132 https://doi.org/10.7717/peerj.10132


## Inspiration and core of this code

Arun Ponnusamy [blog post](https://www.arunponnusamy.com/yolo-object-detection-opencv-python.html) and [code repository](https://github.com/arunponnusamy/object-detection-opencv).

