import pandas as pd
import csv
import datetime
from pathlib import Path


n_first_classes = 9  # from 80


def read_classes(classes_file = 'yolov3.txt'):

    with open(classes_file, 'r') as f:
        classes = [line.strip() for line in f.readlines()]

    return classes


def write_count(file_name, data, append=True):

    if not Path(file_name).is_file() or not append:  # write header
        with open(file_name, mode='w') as file:
            writer = csv.writer(file)
            header = ['date'] + read_classes()[:n_first_classes]
            writer.writerow(header)

            if data is None:
                return None
            if not append:
                for d in data:
                    writer.writerow(d[:n_first_classes + 1])
                return None

    #mode = 'a' if append else 'w'
    with open(file_name, mode='a') as file:
        writer = csv.writer(file)
        for d in data:
            writer.writerow(d[:n_first_classes + 1])



def read_count(file):
    #file = 'data/' + file
    dateparse = lambda x: datetime.datetime.strptime(x, '%Y-%m-%d_%H-%M')
    df = pd.read_csv(file, parse_dates=['date'], date_parser=dateparse)

    return df


def tmp():
    if False:
        date = datetime.date(2016,1,1)
        day1 = datetime.timedelta(days=1)
        c=0
        while date < datetime.date(2017,1,1):
            date=date+day1
            c+=1
        print(c, date)
    d = read_count('data/dane.csv')


def temp1():
    c = read_classes()
    print(len(c), c[:20])
    write_count('data/dane3.csv', 3)


if __name__ == '__main__':
    pass

