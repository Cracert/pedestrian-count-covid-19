
def camera():

    data = {
            'All Saints Square': {
                    'folder' : 'krakow_cam',
                    'appendix' : 'some.stream_',
                    'file' : 'all_saints_square',
                    'first_frame': 1,
                    'last_frame': 6
                    },
            'Grodzka': {
                    'folder' : 'krakowsan',
                    'appendix' : 'some.stream_',
                    'file' : 'grodzka',
                    'first_frame': 2,
                    'last_frame': 6
                    },
            'Wawel': {
                    'folder' : 'krakow_cam',
                    'appendix' : 'some.stream_',
                    'file' : 'wawel',
                    'first_frame': 3,
                    'last_frame': 6
                    },
            'Podgorze Market Square': {
                    'folder' : 'krakow_cam',
                    'appendix' : 'some.stream_',
                    'file' : 'podgorze_market_square',
                    'first_frame': 0,
                    'last_frame': 6
                    }
    }

    return data


if __name__ == '__main__':

    x=camera()
    print(x)